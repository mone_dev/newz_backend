﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using NewZ_Backend.DB_Entity;
using NewZ_Backend.Entities;
using NewZ_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using static NewZ_Backend.Models.Models;

namespace NewZ_Backend.Provider
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            Client client = null;
            
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
                context.TryGetFormCredentials(out clientId, out clientSecret);

            if (context.ClientId == null)
            {
                context.Validated();
                return Task.FromResult<object>(null);
            }

            using (AuthRepository _repo = new AuthRepository())
            {
                client = _repo.FindClient(context.ClientId);
            }

            if (client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if (client.ApplicationType == ApplicationTypes.NativeConfidential)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {

                }
                else if (client.Secret != Helper.GetHash(clientSecret))
                {
                    context.SetError("inviled_clientId", "Client secret is invalid.");
                    return Task.FromResult<Object>(null);
                }
            }

            if (!client.Active)
            {
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set<string>("as:clientAllowOrigin", client.AllowedOrigin);
            context.OwinContext.Set<string>("as:clientRefreshTokenLiftTime", client.RefreshTokenLifeTime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);

        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("asclientAllowedOrigin");
            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin",new[] {allowedOrigin });

            R_LoginModel accountservices = new AuthRepository().Login(context.UserName, context.Password);

            if (accountservices.loggedin[0] && accountservices.loggedin[1] && accountservices.loggedin[2])
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim("username", context.UserName));
                identity.AddClaim(new Claim("customerid", Convert.ToString(accountservices.CustomerID)));
                identity.AddClaim(new Claim("ipaddress", context.Request.RemoteIpAddress));

                var props = new AuthenticationProperties(new Dictionary<string, string> {
                    {
                        "as:client_id",(context.ClientId==null)?string.Empty:context.ClientId
                    },
                    {
                        "username",context.UserName
                    }
                });
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
                await Task.FromResult<object>(null);
            }
            else if (!accountservices.loggedin[0])
            {
                context.SetError("invalid_grant", "Provided username is incorrect");
                return;
            }
            else if (!accountservices.loggedin[2])
            {
                context.SetError("invalid_grant", "Provided username is banned");
                return;
            }
            else
            {
                context.SetError("invalid_grant", "Provided password is incorrect");
                return;
            }

        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if(originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            //Change auth ticket
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            var newClaim = newIdentity.Claims.Where(c => c.Type == "newClaim").FirstOrDefault();
            if(newClaim != null)
            {
                newIdentity.RemoveClaim(newClaim);
            }
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));
            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);
            return Task.FromResult<object>(null);
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach(KeyValuePair<string,string>property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
    }
}