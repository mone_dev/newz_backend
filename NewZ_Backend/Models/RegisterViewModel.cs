﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewZ_Backend.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Username is Requied")]//invalid_register_2
        [StringLength(32, MinimumLength = 4, ErrorMessage = "Username must be between 4 and 32 characters")]////invalid_register_3
        [RegularExpression(@"^.*(?=.*[A-Za-z0-9!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "Username is Invalid")]////invalid_register_4
        public string email { get; set; }

        /*
        [Required(ErrorMessage = "Security is Requied")]////invalid_register_5
        [StringLength(10, MinimumLength = 4, ErrorMessage = "SecurityToken must be between 4 and 10 characters")]////invalid_register_6
        [RegularExpression("[A-Za-z0-9_-]*", ErrorMessage = "SecurityToken is Invalid")]////invalid_register_7
        public string SecurityToken { get; set; }
        */

        [Required(ErrorMessage = "Password is Requied")]////invalid_register_8
        [StringLength(16, MinimumLength = 4, ErrorMessage = "Password must be between 4 and 16 characters")]////invalid_register_9
        [RegularExpression(@"^.*(?=.*[A-Za-z0-9!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "Password is Invalid")]////invalid_register_10
        public string password { get; set; }

        [Required(ErrorMessage = "Confirm Password is Requied")]////invalid_register_11
        [StringLength(16, MinimumLength = 4, ErrorMessage = "Confirm password must be between 4 and 16 characters")]//invalid_register_12
        [RegularExpression(@"^.*(?=.*[A-Za-z0-9!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "Confirm password is Invalid")]//invalid_register_13
        [Compare("password", ErrorMessage = "Password does not match")]//invalid_register_14
        public string confirm_password { get; set; }
    }
}