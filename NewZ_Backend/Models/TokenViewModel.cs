﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewZ_Backend.Models
{
    public class TokenViewModel
    {
        [Required]
        public string tokenId { get; set; }
    }
}