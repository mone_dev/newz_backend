﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NewZ_Backend.DB_Entity
{
    [Table("[dbo].[UsersData]")]
    public class UsersData
    {
        [Key]
        public int CustomerID { get; set; }
        public int IsDeveloper { get; set; }
        public int IsMod { get; set; }
        public int AccountType { get; set; }
        public int AccountStatus { get; set; }
        public int GamePoints { get; set; }
        public int GameDollars { get; set; }
        public DateTime dateregistered { get; set; }
        public DateTime lastjoineddate { get; set; }
        public DateTime lastgamedate { get; set; }
    }
}