﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NewZ_Backend.DB_Entity
{
    [Table("[dbo].[Accounts]")]
    public class Accounts
    {
        [Key]
        public int CustomerID { get; set; }
        public string email { get; set; }
        public string MD5Password { get; set; }
        public string SecurityToken { get; set; }
        public int AccountStatus { get; set; }

        public DateTime dateregistered { get; set; }
        public int ReferralID { get; set; }//0
        public DateTime lastlogindate { get; set; }//datetime
        public string lastloginIP { get; set; }//datetime
        public string InvitedBy { get; set; }//null

    }
}