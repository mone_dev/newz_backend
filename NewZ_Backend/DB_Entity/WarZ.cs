﻿using NewZ_Backend.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NewZ_Backend.DB_Entity
{
    public class WarZ:DbContext
    {
        public WarZ(): base("WarZ")
        {

        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Accounts> tbl_accounts { get; set; }
        public DbSet<UsersData> tbl_userdata { get; set; }
    }
}