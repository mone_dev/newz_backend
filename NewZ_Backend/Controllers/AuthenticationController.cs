﻿using NewZ_Backend.DB_Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewZ_Backend.Controllers
{
    [RoutePrefix("api/Authentication")]
    [Authorize]
    public class AuthenticationController : ApiController
    {
        private AuthRepository _repo = null;
        public AuthenticationController()
        {
            _repo = new AuthRepository();
        }

        [HttpGet]
        [Route("user_info")]
        public async Task<IHttpActionResult> user_info()
        {

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            int id = Convert.ToInt32(claims.Where(c => c.Type == "customerid").Select(p => p.Value).First());

            UsersData _accountservices = _repo.user_info(id);
            object data = new
            {
                _accountservices.dateregistered,
                _accountservices.lastgamedate,
                _accountservices.lastjoineddate,
                _accountservices.GameDollars,
                _accountservices.GamePoints,
            };

            return await Task.FromResult(Ok(data));
        }
    }
}
