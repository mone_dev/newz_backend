﻿using NewZ_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewZ_Backend.Controllers
{
    [AllowAnonymous]
    public class GuestController : ApiController
    {
        private AuthRepository _repo = null;
        public GuestController()
        {
            _repo = new AuthRepository();
        }

        [Route("register")]
        public IHttpActionResult Register(RegisterViewModel userModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            return Ok(_repo.Register(userModel));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _repo.Dispose();
            base.Dispose(disposing);
        }
    }
}
