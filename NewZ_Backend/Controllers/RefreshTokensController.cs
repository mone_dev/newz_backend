﻿using NewZ_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewZ_Backend.Controllers
{
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private AuthRepository _repo = null;

        public RefreshTokensController()
        {
            _repo = new AuthRepository();
        }
        [HttpPost]
        [Route("delete")]
        public async Task<IHttpActionResult>delete(TokenViewModel model)
        {
            var result = await _repo.RemoveRefreshToken(model.tokenId);
            if (result)
                return Ok();
            return BadRequest("Token Id does not exist");
        }
    }
}
