﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NewZ_Backend.Stored_Procedures
{
    public class WarZ_Stored
    {
        string constr = ConfigurationManager.ConnectionStrings["WarZ"].ConnectionString;
        public string FN_CreateMD5Password(string password)
        {
            using (SqlConnection conObj = new SqlConnection(constr))
            {
                SqlCommand comObj = new SqlCommand("[dbo].[FN_CreateMD5Password]", conObj);
                comObj.CommandType = CommandType.StoredProcedure;
                comObj.Parameters.Add(new SqlParameter("@in_Password", password));

                SqlParameter out_MD5 = new SqlParameter("@out_MD5", SqlDbType.VarChar, 32);
                out_MD5.Direction = ParameterDirection.Output;

                comObj.Parameters.Add(out_MD5);
                conObj.Open();

                try
                { comObj.ExecuteNonQuery(); }
                catch
                {
                    conObj.Close();
                    return "ERROR";
                }
                conObj.Close();
                return (comObj.Parameters["@out_MD5"].Value.ToString());
            }
        }

        public bool WZ_ACCOUNT_CREATE(string email, string password)
        {
            using (SqlConnection conObj = new SqlConnection(constr))
            {
                SqlCommand comObj = new SqlCommand("[dbo].[WZ_ACCOUNT_CREATE]", conObj);
                comObj.CommandType = CommandType.StoredProcedure;
                comObj.Parameters.Add(new SqlParameter("@in_IP", "0.0.0.0"));
                comObj.Parameters.Add(new SqlParameter("@in_Email", email));
                comObj.Parameters.Add(new SqlParameter("@in_Password", password));

                comObj.Parameters.Add(new SqlParameter("@in_SecurityToken", "adminz"));
                comObj.Parameters.Add(new SqlParameter("@in_ReferralID", "0"));
                comObj.Parameters.Add(new SqlParameter("@in_SerialKey", " "));
                comObj.Parameters.Add(new SqlParameter("@in_SerialEmail", " "));
                comObj.Parameters.Add(new SqlParameter("@in_RecommendedBy", " "));

                conObj.Open();
                try
                {
                    var dataReader = comObj.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);
                    int value = Convert.ToInt32(dataTable.Rows[0].ItemArray[0]);
                    conObj.Close();
                    return value == 0 ? true : false;
                }
                catch
                {
                    conObj.Close();
                    return false;
                }
            }
        }
    }
}