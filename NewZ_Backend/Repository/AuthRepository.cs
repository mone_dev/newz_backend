﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NewZ_Backend.DB_Entity;
using NewZ_Backend.Entities;
using NewZ_Backend.Models;
using NewZ_Backend.Stored_Procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace NewZ_Backend
{
    public class AuthRepository : IDisposable
    {
        private WarZ _ctx;
        private WarZ_Stored  _stored_warz;
        //  private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new WarZ();
            _stored_warz = new WarZ_Stored();

        //    _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public Client FindClient(string clientId)
        {
            var client = _ctx.Clients.Find(clientId);

            return client;
        }

        public UsersData user_info(int CustomerID)
        {
            var find = from p in _ctx.tbl_userdata.Where(p => p.CustomerID == CustomerID) select p;
            if (find != null)
            {
                UsersData rawdata = find.FirstOrDefault();
                _ctx.Dispose();
                return rawdata;
            }
            return null;
        }

        public R_LoginModel Login(string username, string password)
        {
            bool[] loggedin = new bool[3];
            var find = from p in _ctx.tbl_accounts.Where(p => p.email == username) select p;
            if (find.Any())
            {
                loggedin[0] = true;
                if (find.FirstOrDefault().AccountStatus == 200)
                    loggedin[2] = false;
                else
                    loggedin[2] = true;

                string MD5_pw = _stored_warz.FN_CreateMD5Password(password);
                if (!MD5_pw.Equals("ERROR") && find.Where(p => p.MD5Password == MD5_pw).Any())
                {
                    loggedin[1] = true;
                }
                R_LoginModel model1 = new R_LoginModel
                {
                    CustomerID = find.FirstOrDefault().CustomerID,
                    loggedin = loggedin
                };
                return model1;
            }
            loggedin[0] = false;
            loggedin[1] = false;
            loggedin[2] = false;

            R_LoginModel model = new R_LoginModel
            {
                CustomerID = 0,
                loggedin = loggedin
            };

            _ctx.Dispose();
            return model;

        }

        public bool Register(RegisterViewModel userModel)
        {           
            return _stored_warz.WZ_ACCOUNT_CREATE(userModel.email,userModel.password);
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken = _ctx.RefreshTokens.Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId).SingleOrDefault();
            if(existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }
            _ctx.RefreshTokens.Add(token);
            return await _ctx.SaveChangesAsync() > 0;
        }
        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);
            if (refreshToken != null)
            {
                _ctx.RefreshTokens.Remove(refreshToken);
                return await _ctx.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.RefreshTokens.Remove(refreshToken);
            return await _ctx.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);
            return refreshToken;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            
        }
    }
}